const Koa = require('koa')

module.exports = ({
  config,
  logger,
  cors,
  helmet,
  requestLogger,
  responseTime,
  requestId,
  router,
  public
}) => {
  const server = new Koa()
  server.proxy = true

  server
    .use(requestId)
    .use(responseTime)
    .use(requestLogger)
    .use(cors)
    .use(helmet)
    .use(public)
    .use(router.routes())

  const start = () => {
    logger.info(`API listening on port ${config.port}`)
    return server.listen(config.port)
  }

  return { server, start }
}
