const requestId = require('koa-requestid')

module.exports = () => {
  return requestId()
}
