module.exports = {
  logLevel: process.env.LOG_LEVEL || 'debug',
  cors: process.env.HTTP_CORS || '*',
  port: process.env.HTTP_PORT || 8081,
  numberOfLetters: 9
}
