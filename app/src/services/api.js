import SwaggerClient from 'swagger-client'

const api = {}
new SwaggerClient(`${process.env.VUE_APP_COUNTDOWN_API}/openapi.yml`)
  .then(client => {
    Object.assign(api, client.apis.default)
  })
  .catch(console.error)

export default api
