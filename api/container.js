const {
  createContainer,
  asValue,
  asFunction
} = require('awilix')

const config = require('./config')
const logger = require('./logger')
const {
  server,
  router
} = require('./http')
const {
  cors,
  helmet,
  requestLogger,
  responseTime,
  requestId,
  public
} = require('./middleware')
const {
  letters
} = require('./application')

const container = module.exports = createContainer()

// Bootstrap
container.register({
  config: asValue(config),
  logger: asFunction(logger)
})

// Application
container.register({
  letters: asFunction(letters)
})

// Http
container.register({
  server: asFunction(server),
  router: asFunction(router),
  cors: asFunction(cors),
  helmet: asFunction(helmet),
  requestLogger: asFunction(requestLogger),
  responseTime: asFunction(responseTime),
  requestId: asFunction(requestId),
  public: asFunction(public)
})
