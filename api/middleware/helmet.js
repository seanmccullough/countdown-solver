const helmet = require('koa-helmet')

module.exports = () => {
  return helmet()
}
