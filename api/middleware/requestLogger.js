const requestLogger = require('koa-logger')

module.exports = ({ logger }) => {
  return requestLogger(logger)
}
