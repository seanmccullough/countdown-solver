import Vue from 'vue'
import Vuex from 'vuex'

import { api } from '../services'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    solvingLetters: false,
    highestScore: ''
  },
  mutations: {
    setSolvingLetters (state, value) {
      state.solvingLetters = Boolean(value)
    },
    clearLetters (state) {
      state.highestScore = ''
    },
    setHighestScore (state, value) {
      state.highestScore = value
    }
  },
  actions: {
    async solveLetters ({ commit }, letters) {
      commit('clearLetters')
      commit('setSolvingLetters', true)
      const res = await api.solveLetters({
        letters
      })
      commit('setHighestScore', res.obj.highestScore)
      commit('setSolvingLetters', false)
    }
  },
  modules: {
  }
})
