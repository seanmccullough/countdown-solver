const container = require('./container')
const { server, logger } = container.cradle

process.on('unhandledRejection', err => {
  logger.error('process.unhandledRejection', err)
  process.exit(1)
})

server.start()
