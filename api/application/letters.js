const fs = require('fs')
const wordListPath = require('word-list')
const words = fs.readFileSync(wordListPath, 'utf8').split('\n')

const dictionary = new Map(
  words
    .filter(s => s.length <= 9)
    .map(s => [s.split('').sort().join(''), s])
)

const permutate = str => {
  const product = []
  let counter = 0b0

  while (counter < Math.pow(2, str.length)) {
    const permutation = Array.from(str, (v, i) => Math.pow(2, i) & counter)
      .map((v, i) => v && str[i])
      .filter(Boolean)
      .join('')
    product.push(permutation)
    counter++
  }

  return product
    .sort((a, b) => b.length - a.length)
    .filter(Boolean)
}

module.exports = () => {
  const highestScore = needle => {
    const permutations = permutate(needle.toLowerCase().split('').sort().join(''))

    for (let permutation of permutations) {
      if (dictionary.has(permutation)) return dictionary.get(permutation)
    }

    return null
  }

  return {
    highestScore
  }
}
