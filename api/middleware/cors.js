const cors = require('@koa/cors')

module.exports = ({ config }) => {
  return cors({
    origin: config.cors,
    allowMethods: ['GET'],
    allowHeaders: ['Content-Type'],
    keepHeadersOnError: true
  })
}
