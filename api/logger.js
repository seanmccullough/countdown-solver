const winston = require('winston')
const {
  combine,
  colorize,
  timestamp,
  align,
  simple
} = winston.format

module.exports = ({ config }) => {
  return winston.createLogger({
    level: config.logLevel,
    format: combine(
      colorize(),
      timestamp(),
      align(),
      simple()
    ),
    transports: [
      new winston.transports.Console({
        prettyPrint: true,
        colorize: true
      })
    ]
  })
}
