const Router = require('koa-router')
const HttpStatus = require('http-status-codes')

module.exports = ({ config, letters }) => {
  const router = new Router()

  router.get('/health', (ctx, next) => {
    ctx.body = { status: 'UP' }
    return next()
  })

  router.get('/letters', async (ctx, next) => {
    ctx.body = {}
    const input = ctx.query.letters || ''
    if (input.length > config.numberOfLetters) {
      ctx.status = HttpStatus.BAD_REQUEST
      ctx.body.message = `letters param must be exactly ${config.numberOfLetters} characters`
      return next()
    }
    ctx.body.highestScore = await letters.highestScore(input)
    return next()
  })

  return router
}
