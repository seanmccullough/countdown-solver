const serve = require('koa-static')

module.exports = () => {
  return serve('public')
}
